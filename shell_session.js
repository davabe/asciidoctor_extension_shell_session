function copyToClipboard(text) {
  var temp = document.createElement("textarea");
  document.getElementsByTagName("body")[0].appendChild(temp);
  temp.value = text
  temp.select();
  document.execCommand("copy");
  temp.remove();
}
