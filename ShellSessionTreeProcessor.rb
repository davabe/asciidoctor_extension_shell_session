require 'asciidoctor/extensions'
require 'cgi'
require 'fileutils'

include Asciidoctor

Extensions.register do
  treeprocessor ShellSessionTreeProcessor if document.attributes['backend'] =~ /^html/
  docinfo_processor ShellSessionDocinfoProcessor if document.attributes['backend'] =~ /^html/
end

class ShellSessionDocinfoProcessor < Extensions::DocinfoProcessor
  def process document
    logger = ::Asciidoctor::LoggerManager.logger

    logger.info("Compiling ShellSession.js")

    extdir = ::File.join(::File.dirname __FILE__)
    stylesheet_name = 'shell_session.css'
    js_name = 'shell_session.min.js'

    output = []

    if document.attr? 'linkcss'
      stylesheet_href = handle_stylesheet document, extdir, stylesheet_name
      output << [%{<link rel="stylesheet" href="#{stylesheet_href}">}]
    else
      content = document.read_asset %(#{extdir}/#{stylesheet_name})
      output << ['<style>', content.chomp, '</style>']
    end

    if document.attr? 'linkjs'
      js_href = handle_js document, extdir, js_name
      output << [%(<script type="text/javascript" src="#{js_href}"></script>)]
    else
      js = document.read_asset %(#{extdir}/#{js_name})
      js.strip!
      output << ['<script type="text/javascript">', js.chomp, '</script>']
    end

    output * "\n"
  end

  def handle_stylesheet doc, extdir, stylesheet_name
    logger = ::Asciidoctor::LoggerManager.logger
    outdir = (doc.attr? 'outdir') ? (doc.attr 'outdir') : (doc.attr 'docdir')
    stylesoutdir = doc.normalize_system_path((doc.attr 'stylesdir'), outdir, (doc.safe >= SafeMode::SAFE ? outdir : nil))
    if stylesoutdir != extdir && doc.safe < SafeMode::SECURE && (doc.attr? 'copycss')
      destination = doc.normalize_system_path stylesheet_name, stylesoutdir, (doc.safe >= SafeMode::SAFE ? outdir : nil)
      create_path(destination)
      content = doc.read_asset %(#{extdir}/#{stylesheet_name})
      ::File.open(destination, 'w') {|f|
        f.write content
      }
      destination
    end
    File.join(".", (doc.attr 'stylesdir') || "", stylesheet_name)
  end

  def handle_js doc, extdir, js_name
    outdir = (doc.attr? 'outdir') ? (doc.attr 'outdir') : (doc.attr 'docdir')
    jsoutdir = doc.normalize_system_path((doc.attr 'jsdir'), outdir, (doc.safe >= SafeMode::SAFE ? outdir : nil))
    if jsoutdir != extdir && doc.safe < SafeMode::SECURE && (doc.attr? 'copyjs')
      destination = doc.normalize_system_path js_name, jsoutdir, (doc.safe >= SafeMode::SAFE ? outdir : nil)
      create_path(destination)
      content = doc.read_asset %(#{extdir}/#{js_name})
      ::File.open(destination, 'w') {|f|
        f.write content
      }
      destination
    end
    File.join(".", (doc.attr 'jsdir') || "", js_name)
  end

  def create_path(some_path)
    dirname = File.dirname(some_path)
    unless File.directory?(dirname)
      FileUtils.mkdir_p(dirname)
    end
  end
end

# A treeprocessor extension that identifies literal blocks that appear to be
# commands in a shell session and converts them to listing blocks with a
# terminal style.
class ShellSessionTreeProcessor < Extensions::TreeProcessor
  LF = ?\n

  def process document
    logger = ::Asciidoctor::LoggerManager.logger

    logger.info("Compiling ShellSession")


    (document.find_by(context: :literal) {|candidate| candidate.lines[0].start_with? '$ ', '> ', '# '}).each do |block|
      (children = block.parent.blocks)[children.index block] = convert_to_terminal_listing block
    end

    nil
  end

  def convert_to_terminal_listing block
    attrs = block.attributes
    attrs['role'] = 'terminal'
    prompt_attr = (attrs.key? 'prompt') ?
        %( data-prompt="#{block.sub_specialchars attrs['prompt']}") : nil

    if attrs['subs'] && attrs['subs'].include?("attributes")
      attrs['subs'] = "attributes"
    else
      attrs['subs'] = nil
    end

    copyText = ""

    lines = (block.content.split LF).map do |line|
      case
      when line.start_with?('$')
        l = (line[2..-1] || "").strip
        copyText += l + "\\r\\n"
        %(<span class="command"#{prompt_attr}>#{l}</span>)
      when line.start_with?('&gt;')
        %(<span class="output">#{(line[5..-1] || "").lstrip()}</span>)
      when line.start_with?('>')
        %(<span class="output">#{(line[1..-1] || "").lstrip()}</span>)
      when line.start_with?('#')
        %(<span class="comment">#{line}</span>)
      else
        line
      end
    end

    unless copyText.empty?
      lines << %(<i onclick="copyToClipboard('#{copyText}')" class="fa fa-clipboard clipboard" aria-hidden="true"></i>)
    end

    new_block = create_listing_block block.parent, lines * LF, attrs, subs: attrs['subs']
    new_block.title = block.title
    new_block
  end
end
