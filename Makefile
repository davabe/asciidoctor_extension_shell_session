DOCKER_ASCIIDOCTOR_IMAGE=freke/docker_asciidoctor

PWD=$(shell pwd)
UID=$(shell id -u)
GID=$(shell id -g)

DOCKER=docker run -v "$(PWD)":/documents -u "$(UID)" --rm $(DOCKER_ASCIIDOCTOR_IMAGE)
ASCIIDOCTOR=$(DOCKER) asciidoctor

SRC_FILE=$(wildcard *.adoc)
HTML_OBJS = $(SRC_FILE:.adoc=.html)

all: $(HTML_OBJS)

%.html: %.adoc
	@echo "\n\nBuilding $<"
	$(ASCIIDOCTOR) -a stylesdir=css -a jsdir=js -a linkjs -a linkcss -a icons=font -a copyjs -D out -r ./ShellSessionTreeProcessor.rb $<
	@echo "Done $<\n"

clean:
	rm -Rf out
